package com.fanwei.construction;

/**
 * @author fanwei
 * @version 1.0.0
 * @ClassName Person.java
 * @Description Person
 * @createTime 2022年04月08日 17:24:00
 */
public class Person implements Comparable<Person> {

    private String name;
    private int age;

    public Person() {
    }

    public Person(int age) {
        this.age = age;
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public int compareTo(Person person) {
        return age - person.getAge();
    }

    @Override
    public String toString() {
        return age+"";
    }

//    @Override
//    public String toString() {
//        return "Person{" +
//                "name='" + name + '\'' +
//                ", age=" + age +
//                '}';
//    }
}
