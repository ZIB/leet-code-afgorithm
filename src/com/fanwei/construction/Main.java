package com.fanwei.construction;


import com.fanwei.construction.printer.BinaryTrees;

import java.util.Comparator;

/**
 * @author fanwei
 * @version 1.0.0
 * @ClassName Main.java
 * @Description TODO
 * @createTime 2022年04月08日 16:08:00
 */
public class Main {

    private static class PersonComparator implements Comparator<Person>{
        @Override
        public int compare(Person o1, Person o2) {
            return o1.getAge()- o2.getAge();
        }
    }

    public static void main(String[] args) {
//        test2();
//        test3();
//        test4();
//        test5();
        test6();
    }

    /**
     * 打压这颗bst结构
     */
    static void test2() {
        Integer data[] = new Integer[] {
                7, 4, 9, 2, 5, 8, 11, 3, 12, 1
        };

        BinarySearchTree<Person> bst1 = new BinarySearchTree<>(new PersonComparator());
        for (int i = 0; i < data.length; i++) {
            bst1.add(new Person(data[i]));
        }

        BinaryTrees.println(bst1);
    }

    /**
     * 前序遍历
     */
    static void test3(){
        Integer data[] = new Integer[] {
                7, 4, 9, 2, 5
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }

        BinaryTrees.println(bst);
        System.out.println("------------------------------");
        bst.proOrderTraversal();

    }

    /**
     * 中序遍历
     */
    static void test4(){
        Integer data[] = new Integer[] {
                7, 4, 9, 2, 5, 8, 11, 3, 12, 1
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }
        BinaryTrees.println(bst);
        System.out.println("------------------------------");
        bst.inOrderTraversal();
    }


    /**
     * 后序遍历
     */
    static void test5(){
        Integer data[] = new Integer[] {
                7, 4, 9, 2, 5, 8, 11, 3, 12, 1
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }
        BinaryTrees.println(bst);
        System.out.println("------------------------------");
        bst.postorderTraversal();
    }

    /**
     * 中层遍历
     */
    static void test6(){
        Integer data[] = new Integer[] {
                7, 4, 9, 2, 5, 8, 11, 3, 12, 1
        };

        BinarySearchTree<Integer> bst = new BinarySearchTree<>();
        for (int i = 0; i < data.length; i++) {
            bst.add(data[i]);
        }
        BinaryTrees.println(bst);
        System.out.println("------------------------------");
        bst.levelOrderTraversal();
    }
}
