package com.fanwei.afgorithm.code20;

import java.util.Stack;

/**
 * @author fanwei
 * @version 1.0.0
 * @ClassName ValidParentheses.java
 * @Description TODO
 * @createTime 2022年03月03日 18:54:00
 */
public class ValidParentheses {

    public boolean isValid(String s) {
        int str_len = s.length();
        Stack<Character> set = new Stack<>();
        for (int i = 0; i < str_len;i++){
            char c = s.charAt(i);
            if (c == '(' || c == '[' || c == '{'){ //左侧字符串
                set.push(c);
            }else {//右边拿来退避
                if (set.isEmpty()) return false;
                char result = set.pop();
                if (result == '(' && c != ')' ){
                    return false;
                } else if (result == '[' && c != ']' ){
                    return false;
                } else if (result == '{' && c != '}' ) {
                    return false;
                }
            }

        }
        //todo -- 递归出stack为空的时候才为true，整个过程才算完结
        return set.isEmpty();
    }

    public static void main(String[] args) {
        String s = "){";
        ValidParentheses demo = new ValidParentheses();
        System.out.println(demo.isValid(s));
    }
}
