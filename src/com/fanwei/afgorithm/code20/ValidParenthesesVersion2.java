package com.fanwei.afgorithm.code20;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * @author fanwei
 * @version 1.0.0
 * @ClassName ValidParenthesesVersion2.java
 * @Description 是原来ValidParentheses的优化版本
 * @createTime 2022年03月04日 08:00:00
 */
public class ValidParenthesesVersion2 {

    public static Map<Character,Character> map = new HashMap<>();

    static {
        map.put('(',')');
        map.put('[',']');
        map.put('{','}');
    }

    public boolean isValid(String s){
        Integer len = s.length();
        if (len % 2 != 0) return false;
        char[] arr = s.toCharArray();
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < len; i++){
            if (!map.containsKey(arr[i])){//这里是char不符合左侧前缀，去分析
                if (stack.isEmpty())return false;
                char result = stack.pop(); //获取第一个入stack的字节
                if (arr[i] != map.get(result)){ //判断是否宇通字节那样对齐，不等就返回false
                    return false;
                }
            }else {//这里符合放入stack中,但是如果"){"，就返回false
                if (!map.containsKey(arr[i]))return false;
                stack.push(arr[i]);
            }
        }
        //这里是编译全部通过，就走这里
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        String s = "){";
        System.out.println(new ValidParenthesesVersion2().isValid(s));
    }
}
