package com.fanwei.afgorithm.code3;

import java.util.HashSet;
import java.util.Set;

/**
 * @desc 3. 无重复字符的最长子串
 * https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/
 * 解决方法：https://www.bilibili.com/video/BV1CB4y1K77p/?spm_id_from=333.788.recommend_more_video.3
 * https://labuladong.gitbook.io/algo/mu-lu-ye-1/mu-lu-ye-4/hua-dong-chuang-kou-ji-qiao-jin-jie#si-zui-chang-wu-zhong-fu-zi-chuan
 * b站：
 * @author fanwei
 */
public class LongestSubstringWithoutRepeatingCharacters {

    /**
     * 返回最大的 -- 使用滑动窗口法
     * @param s
     * @return
     */
    public static int lengthOfLongestSubstring(String s) {
        //左index，右index，最大数值
        int left = 0, right = 0, maxWindow = 0;
        //这里是存储是否有相同的字节
        Set<Character> winSet = new HashSet<>();
        while (right < s.length() && left < s.length()){
            //这里是winSet中包含该字符串
            if (winSet.contains(s.charAt(right))){
                winSet.remove(s.charAt(left));
                left++;
            }
            //这里是没包含对应的字节时候
            else {
                winSet.add(s.charAt(right));
                right++;
                int currentLength = right - left;
                maxWindow = Math.max(maxWindow,currentLength);
            }
        }
        return maxWindow;
    }

    public static void main(String[] args) {
        String s = "pwwkew";
        System.out.println(lengthOfLongestSubstring(s));
    }
}
