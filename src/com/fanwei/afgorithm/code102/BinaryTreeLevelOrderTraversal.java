package com.fanwei.afgorithm.code102;

import java.util.ArrayList;
import java.util.List;

/**
 * @author fanwei
 * @version 1.0.0
 * @ClassName 102. 二叉树的层序遍历
 * @Description https://leetcode-cn.com/problems/binary-tree-level-order-traversal/
 * @createTime 2021年11月30日 10:24:00
 */
public class BinaryTreeLevelOrderTraversal {

    public class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) { this.val = val; }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    List<List<Integer>> res = new ArrayList<List<Integer>>();
    public List<List<Integer>> levelOrder(TreeNode root) {
        if (root == null)return res;
        List<TreeNode> nodes = new ArrayList<>();
        nodes.add(root);
        helper(nodes,1);
        return res;
    }

    private void helper(List<TreeNode> nodes, int index) {
        if (nodes.size() == 0) return;
        List<Integer> list = new ArrayList<>();
        List<TreeNode> currNodes = new ArrayList<>();

        for (int i = 0; i < nodes.size(); i++){
            TreeNode node = nodes.get(i);
            list.add(node.val);
            if (node.left != null){
                currNodes.add(node.left);
            }
            if (node.right != null){
                currNodes.add(node.right);
            }

        }

        res.add(list);
        helper(currNodes, index + 1);
    }
}
