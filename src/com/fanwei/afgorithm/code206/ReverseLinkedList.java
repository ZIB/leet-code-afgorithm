package com.fanwei.afgorithm.code206;

/**
 * @author fanwei
 * @version 1.0.0
 * @ClassName ReverseLinkedList.java
 * @Description 206-反转链表
 * @address https://leetcode-cn.com/problems/reverse-linked-list/
 * @createTime 2021年11月01日 23:34:00
 */
public class ReverseLinkedList {

    public class ListNode {
       int val;
       ListNode next;
       ListNode() {}
       ListNode(int val) { this.val = val; }
       ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    /**
     * 这里就是把原来ListNode的链表通过递归倒过来
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head){
        if (head == null || head.next == null){
            return head;
        }
        ListNode newNode = reverseList(head.next);
        head.next.next = head;
        head.next = null;
        return newNode;
    }
}
//https://github.com/jiajunhua/labuladong-fucking-algorithm/blob/master/%E9%AB%98%E9%A2%91%E9%9D%A2%E8%AF%95%E7%B3%BB%E5%88%97/k%E4%B8%AA%E4%B8%80%E7%BB%84%E5%8F%8D%E8%BD%AC%E9%93%BE%E8%A1%A8.md
//https://www.bilibili.com/video/BV11b41157ok?from=search&seid=8915284132449269323&spm_id_from=333.337.0.0


